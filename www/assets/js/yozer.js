/*
--- TODO ---
* display acurate 'from': find the original client
* display attachments
*/

//global variables
apiroot = undefined;
most_recent_dent_id = 0;
most_recent_mention_id = 0;
dent_timeout = 0;
mentions_timeout = 0;
names_from_url = {};
//TODO: read the prune lengths from the settings page
dents_prune_length = 100;
mentions_prune_length = 100;
selected_tab_content = undefined;
//who the fuck am I?
client_name = "yozer"

// the function to run first
$(function(){
	// add the mozilla specific bullshit
	$.ajaxSetup( {
		xhr: function() {
			return new window.XMLHttpRequest( {
				mozSystem: true
			} );
		}
	});

	set_up_main_tabs();
	get_stored_data();
	set_up_settings();
	set_up_new_dent();
});


/* local stored data */
function get_stored_data() {
	// get various data items and store them in global variables?
	username = localStorage["username"];
	password = localStorage["password"];
	//TODO: make sure the password is readable by yozer only
	service = localStorage["service"];

	if(username && password  && service ) {
		authenticate(username, password, service);
	} else {
		if (!service) {
		//if service is undefined, try using the hostname
		var pu = parse_url(document.location.href);
			service = pu.protocol+"//"+pu.hostname;
		}
		//go to the settings page
		$("#settings").click();
	}
}

/* settings */
function set_up_settings() {
	//set info into the settings
	$("#authenticate input[name='service']").val(service);
	$("#authenticate input[name='username']").val(username);
	$("#authenticate input[name='password']").val(password);
	//set an action for the 'submit' button
	$("#authenticate button[name='submit']").on('click', click_authenticate_submit);
	$("#clear_localstorage").on('click', click_clear_localstorage);
}

/* new dent */
function set_up_new_dent() {
	//hide the 'to' stuff
	$("#new_dent_reply_to").hide();
	//add a click listener to the submit button
	$("#new_dent button[name='submit']").on('click', click_new_dent_submit);
	$("#new_dent button[name='clear']").on('click', click_new_dent_clear);
}

function set_apiroot(service) {
	//does the service start with http?
	if( service.indexOf("http://")!=0 ) {
		service = "http://"+service;
	}
	//set the global apiroot
	apiroot = service+"/api";
}

function set_authhash(username,password) {
	auth_hash =	btoa(username + ":" + password);
}

// authenticate rquest
function click_authenticate_submit() {
	var button = $("#authenticate button[name='submit']");
	//button.prop( "disabled", true );
	var service = $("#authenticate input[name='service']").val().trim();
	var username = $("#authenticate input[name='username']").val().trim();
	var password = $("#authenticate input[name='password']").val().trim();
	// do we have service, username and password?
	if (service!='' || username != '' || password!='') {
		//try to authenticate against the service
		authenticate(username, password, service);
	}
}

function authenticate(username, password, service) {
	set_apiroot(service);
	set_authhash(username, password);
	$.ajax ({
		type: 'GET',
		beforeSend: set_auth_headers,
		url: apiroot+"/account/verify_credentials.xml",
		dataType: 'xml',
		crossDomain: true,
		success: function (){
			user_authenticated(username, password, service);
		},
		error: function(xhr, status, error){
			ajax_failure(xhr, status, error);
			alert('Authentication Error');
		}
	});
}

function user_authenticated(username, password, service) {
	//authentication was successful
	authenticated = true;
	//record the data
	localStorage["username"] = username;
	localStorage["password"] = password;
	localStorage["service"] = service;

	//get the dents!
	request_dents();
	request_mentions();
	//click the dents tab
	$("#dents").click();
}

//user clicked new dent submit
function click_new_dent_submit() {
	//disable the input
	var text = $("#new_dent textarea[name='status']").val();
	var text = text.trim();
	if (text != "") {
		//get ready to send a dent!
		$("#new_dent textarea[name='status']").prop('disabled',true);
		request_post_new_dent();
	}
}

//user clicked new dent clear
function click_new_dent_clear() {
	clear_new_dent_form();
}

function clear_new_dent_form() {
	//clear the data
	$("#new_dent textarea[name='status']").val('');
	// add the OG dent's author nickname to the new dent form
	$("#new_dent_reply_to_name").text('');
	//show the new dent reply to
	$("#new_dent_reply_to").hide();
	//empty the 'in reply to'
	$("#in_reply_to").empty();
	//clear the field value
	$("#new_dent input[type='file']").val('');
	//enable the text field
	$("#new_dent textarea[name='status']").prop("disabled", false);

}

function click_reply_button(event) {
	var button = $(event['currentTarget']);
	//get the parent dent of the button
	var dent = $(button).closest(".dent");
	//get the local_id of the dent
	var local_status_id = dent.attr('status_id');
	//get the creeators name
	var reply_to_username = dent.children(".username").text();
	// add the OG dent's author nickname to the new dent form
	$("#new_dent_reply_to_name").text(reply_to_username);
	//show the new dent reply to
	$("#new_dent_reply_to").show();
	// create a hidden 'in reply to' field to the new dent form
	var in_reply_to_field = $("<input type='hidden' name='in_reply_to_status_id' value='"+local_status_id+"'>");
	//clear the 'in_reply_to'
	$("#in_reply_to").empty();
	//add the hidden field
	$("#in_reply_to").append(in_reply_to_field);
	// 'click' the new dent tab
	$("#new_dent").click();

}

function request_post_new_dent() {
	//gather the data
	var fd = new FormData();
	fd.append('status', $("#new_dent textarea[name='status']").val() );
	fd.append('source', client_name);
	if( $("#new_dent input[name='in_reply_to_status_id']")[0] ) {
		fd.append('in_reply_to_status_id', $("#new_dent input[name='in_reply_to_status_id']").val() );
	}
  //why can I not get this info with a jquery selector?
  var f = document.getElementById("file_upload").files[0];
  if (f) {
		fd.append('media', f);
	}

	$.ajax ({
		type: 'POST',
		beforeSend: set_auth_headers,
		url: apiroot+"/statuses/update.xml",
		data: fd,
		dataType: 'xml',
		processData: false,
		contentType: false,
		crossDomain: true,
		success: function (){
			/* dent was posted! */
			//clear the form
			clear_new_dent_form();
		},
		error: function(xhr, status, error){
			ajax_failure(xhr, status, error);
			alert('error posting dent');
			$("#new_dent input[name='dent']").prop("disabled", false);
		}
	});
}

function click_clear_localstorage() {
// clear the localstorage
localStorage["username"] = '';
localStorage["password"] = '';
localStorage["service"] = '';
alert('localStorage has been cleared');

}

function start_dent_timeout() {
	dent_timeout = setTimeout( request_dents, 60000 );
}

function request_name_for_url(url, type) {
	/* parse info from the url */
	//is this a user or group?
	if ( object_type_is_person(type) ) {
		var new_url = url.replace('user/', "api/users/show.json?id=");
		// is this really a group?
		if (url.indexOf('/group/')!=-1) {
			type = 'group'; //this is sloppy
		}
	} else if( object_type_is_group(type)) {
		var new_url = url.replace('group/', "api/statusnet/groups/show/");
		new_url = new_url.replace('/id', '.json');

	} else {
		console.log("Unknown 'to' type: "+type);
		return;
	}
	//try to get the name associated with the user at this url
	$.ajax ({
		type: 'GET',
		url: new_url,
		dataType: 'json',
		crossDomain: true,
		success: function (data){
			process_name_for_url(data, url, type);
		},
		error: function(xhr, status, error){
			ajax_failure(xhr, status, error);
		}
	});
}

function request_dents() {
	//what URL do we need to parse?
	var url = apiroot+"/statuses/friends_timeline/"+localStorage['username']+".as?since="+most_recent_dent_id;
	//try to get some statuses for this user
	$.ajax ({
		type: 'GET',
		beforeSend: set_auth_headers,
		url: url,
		dataType: 'json',
		crossDomain: true,
		success: function (data){
			process_dent_items(data,'dents');
			start_dent_timeout();
		},
		error: function(xhr, status, error){
			ajax_failure(xhr, status, error);
		}
	});
}

function start_mentions_timeout() {
	mentions_timeout = setTimeout( request_mentions, 90000 );
}

function request_mentions() {
	//what URL do we need to parse?
	var url = apiroot+"/statuses/mentions/"+localStorage['username']+".as?since="+most_recent_mention_id;
	//try to get mentions for this user
	$.ajax ({
		type: 'GET',
		beforeSend: set_auth_headers,
		url: url,
		dataType: 'json',
		crossDomain: true,
		success: function (data){
			process_dent_items(data,'mentions');
			start_mentions_timeout();
		},
		error: function(xhr, status, error){
			ajax_failure(xhr, status, error);
		}
	});
}

function set_auth_headers(xhr) {
	xhr.setRequestHeader("Authorization", "Basic "+auth_hash);
}

/* tab related stuff */
function set_up_main_tabs() {
	//hide all of the tab contents
	$(".tab_content").each(function(i,e){
		$(e).hide();
	});
	var tabs = $("#tabs .tab");
	tabs.each(function(i,e){
		var tab = $(e);
		//what happens when a tab is clicked?
		tab.bind('click', click_tab );
	});
}

function click_tab(event) {
	var button = $(this);

	//hide the previous tab
	if (selected_tab_content != undefined) {
		selected_tab_content.hide();
	}
	//remove the selected_tab_content class from the previous button
	$(".selected_tab").removeClass('selected_tab');
	//determine the newly selected tab
	button.addClass('selected_tab');
	var tab_id = button.attr("id");
	selected_tab_content = $("#"+tab_id+"_content");
	selected_tab_content.show();
	if (tab_id == "new_dent") {
		//focus on the textarea
		$("textarea[name='dent']").focus();
	}

}

/*		DATA processing		 */
function process_dent_items(data, type ) {
	//reverse the data array
	var items = data.items.reverse();
	$.each(items, function(i, dent){
		var id = dent['status_net']['notice_info']['local_id'];
		if (type == 'dents' ) {
			if (id > most_recent_dent_id) {
				var d = dent_to_html_thingy(dent, type);
				$("#dents_list").prepend(d);
				most_recent_dent_id = id;
			}
		} else if(type == 'mentions') {
			if (id > most_recent_mention_id) {
				var d = dent_to_html_thingy(dent, type);
				$("#mentions_list").prepend(d);
				most_recent_mention_id = id;
			}
		}
	});
}

function process_name_for_url(data, url, type) {
	if (object_type_is_person(type)) {
		var name = data['screen_name'];
	} else {
		var name = "!"+data['nickname'];
	}
	//add the name to the array
	names_from_url[url] = name;
	//add this name to all the .to_name with url = to_url
	$(".actor_name[actor_name_url='"+url+"']").text(name);
}

function dent_to_html_thingy(dent) {
	//console.log(dent);
	//is there a better/tempately way to do this? this just seems so shitty
	// get the user
	var user = dent['actor'];
	var id = user['id'];

	//create some elements
	var dent_element = $("<div class='dent' status_id='"+dent['status_net']['notice_info']['local_id']+"'>");
	var avatar_element = $("<img class='avatar'>");
	var username_element = $("<span class='username actor_name' actor_name_url='"+id+"'>");
	var published_element = $("<div class='published'>");
	var buttons_element = $("<div class='buttons'>");
	//get data from the dent
	var publish_date = dent['published'];
	var text = dent['content'];
	//TODO: sanitize the text!!!!!!

	//use a thumbname avatar_element
	/* get a URL of the avatarImages */
	var al = user['status_net']['avatarLinks'];
	var avatar_src = al[ al.length-1 ]["url"];

	avatar_element.attr('src', avatar_src);
	// if the user's name and id are not in the URL array, add it!
	if (names_from_url[ id ] == undefined) {
		names_from_url[ id ] = "";
		request_name_for_url(id, 'person');
	} else {
		username_element.text(names_from_url[ id ]);
	}
	published_element.text( publish_date);

	//get dent['to'] each username
	var to = dent['to'];
	//is this dent directed to anyone?
	if (to.length > 0 ) {
		//loop through the tos
		var to_indexes = [];
		$.each(to, function(index, t){
			var type = t['objectType'];
			//is this type a person or group?
			if (object_type_is_group(type) || object_type_is_person(type) ) {
				to_indexes.push(index);
			}
		});
		//are they any to_ids to process?
		if (to_indexes.length > 0) {
			var to_wrapper_element = $("<span class='dent_to'>");
			var to_icon = $("<span class='to_icon'>➣</span>");
			to_wrapper_element.append(to_icon);
			//loop through the to
			for(var i=0; i < to_indexes.length; i++) {
				var index = to_indexes[i];
				var t = to[index];
				var to_url = t['id'];
				var to_element = $("<span class='to_name actor_name' actor_name_url='"+to_url+"'>");
				//try to get the to name from the
				var to_name = names_from_url[ to_url ];
				//does this url exist in the names_from_url list?
				if (to_name != undefined ) {
					//add the name to the to_element
					to_element.text( to_name );
				} else {
					//add the url to the array
					names_from_url[ to_url ] = "";
					request_name_for_url( to_url, t['objectType'] );
				}
				to_wrapper_element.append( to_element );
			}
		}
	}

	//make some button to do shit
	var reply_button = $("<button>Reply</button>");
	reply_button.on('click', click_reply_button);
	buttons_element.append(reply_button);


	//put it all together
	dent_element.append(avatar_element);
	dent_element.append(username_element);
	if(to_wrapper_element) {
		dent_element.append(to_wrapper_element);
	}
	dent_element.append(published_element);

	dent_element.append( text );
	//TODO: sanitize the statusnet_html
	dent_element.append( $("<div class='clearfix'>") );
	//add the buttons
	dent_element.append( buttons_element );
	return dent_element;
}
