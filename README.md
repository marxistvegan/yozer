Yozer
---------

Yozer is a GNU Social Client that doesn't work very well. :)


####Development
The index.html file is created from index.haml, and any changes in the initial DOM
need to be made in the HAML file and then compiled to HTML. Similarly, the CSS is created from a SASS file in the src directory.


####Testing Locally
#####Ubuntu
1. install the ubuntu-sdk
1. `cd` to www directory
2. `ubuntu-html5-app-launcher --www=www --inspector`
3. read the output from the previous command, and direct a webkit based browser accordingly

#####Firefox
1. from the 'developer' menu, open the WebIDE
2. in the WebIDE, install a simulator
3. select the new simulator as your Runtime
4. In the Menu: Project>Open Project App
5. Select the www directory of Yozer
6. In the Menu: Project>Install and Run (a firefoxos emulator will launch)
7. In Service disregard http or https and just type the name of the server i.e. social.mayfirst.org followed by the Name and Password then Press Submit
8. in the Menu: Project>Debug App / F12

####Testing in Production
1. put the contents of the 'www' directory in a publicly accessible subdirectory of a GNU Social instance
2. access the subdirectory with a decent web browser
